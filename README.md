
### Description
This is a simple app that built by node.js technology using express.js framework. This app will summarize your google spreadsheet data on the personal dashboard view.
Basically, the stack of this app is: 

+ node.js (using ejs templating system) to generate the frontend view
+ node.js (with express.js framework as the backend system)
+ google spreadsheet as the source of data (somewhat similar with db system)

### Requirements
To run the app you need some requirements to play along with.

+ google spreadsheet as your source of data
+ google credentials ([How to get your google cred](https://docs.wso2.com/display/IntegrationCloud/Get+Credentials+for+Google+Spreadsheet))
+ update your cred on config/config.js
+ follow the data structure below (or if you want to customize your own, change the logic in routes/investment.js file)

### Data Structure
Below is the sample of how I made the data on google spreadsheet:

| Ref ID        | Type      | Post Date       | Start Loan Date | Due Date   | Actual Length (Days)  | Amount  | Amount Return | Status  |
| ------------- |:---------:| ---------------:|:---------------:|:----------:|:---------------------:|:-------:|:-------------:|:-------:|
| ABC-123-DEF   |Investment | 10 Dec 2018     | 11 Dec 2018     | 1 Jan 2019 | 20 Days               | 50 US$  | 55 US$        | Offered |

But of course you can customize whatever you like!

### Disclaimer
+ This is only a sample app, to figure how node.js can work with google spreadsheet. Not or maybe haven't be a real app :peace
+ Dashboard template created by creative team, you can refer to their awesome work [here](https://www.creative-tim.com/product/now-ui-dashboard)