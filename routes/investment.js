const express     = require('express');
const investment  = express.Router();

const { google }  = require('googleapis');
const sheets      = google.sheets('v4');

// Taking parameters from your configuration
const config = require('../config/config');

investment.get('/', (req, res) => {
  console.log('Inside the real handler for our dashboard');
  
  authorize((authClient) => {
    let request = {
      // The ID of the spreadsheet to retrieve data from.
      spreadsheetId         : '1y_uYYqyU3WDW0WRxjNxjR6wSuIBAXHH5G1ohtujp0IQ',
      range                 : 'INVESTMENT BALANCE!A2:J100',
      valueRenderOption     : 'FORMATTED_VALUE',
      dateTimeRenderOption  : 'FORMATTED_STRING',
      auth                  : authClient
    };

    sheets.spreadsheets.values.get(request, (error, response) => {
      if (error) {
        console.error(error);
        return
      }

      let total_income = 0;

      // Transform to Array JSON format
      let dataArr       = [];
      let dataJson      = {};
      let rawData       = response.data.values;
      let successInvest = 0;

      for(let i = 0; i < rawData.length; i++) {
        dataJson = {
          ref_id        :  rawData[i][0],
          type          :  rawData[i][1],
          post_date     :  rawData[i][2],
          loan_date     :  rawData[i][3],
          due_date      :  rawData[i][4],
          actual_length :  rawData[i][5] != '' ? parseInt(rawData[i][5]) : '-',
          rate          :  rawData[i][6] != '' ? rawData[i][6] : '-',
          amount        :  rawData[i][7] != '' ? parseInt(rawData[i][7]) : '-',
          return        :  rawData[i][8] != '' || NaN  ? parseInt(rawData[i][8]) : '-',
          status        :  rawData[i][9]
        };
        
        dataArr.push(dataJson);

        // POPULATE VALUES W.R.T AMOUNT BALANCE
        if(rawData[i][9] === "Paid") {
          total_income += parseInt(rawData[i][8]);
          successInvest++;
        }
      }

      // Count Paid in a month
      let arr_paid_month = [];
      let newDate;

      dataArr.find((el) => {
        if(el.status == "Paid") {
          newDate = new Date(el.due_date);
          arr_paid_month.push(newDate.getMonth());
        }
      });

      let total_unique_month = countUnique(arr_paid_month);

      // As Placeholder for the JSON Contract
      let jsonResponse = {
        avg_per_month   : convertToRupiah(Math.floor(total_income/total_unique_month)),
        avg_per_invest  : convertToRupiah(Math.floor(total_income/successInvest)),
        list_data       : dataArr,
        total_income    : convertToRupiah(total_income)
      };

      console.log(jsonResponse);
      res.render('dashboard', {
        investments : jsonResponse
      });
    });
  });
});

investment.get('/add_data', (req, res) => {
  console.log("Add data to the sheet");
  // API FOR ADD DATA TO THE SPREADSHEET
  res.json({"success": true});
});

function authorize(callback) {
  let authClient = config.authSettings;

  if (authClient == null) {
    console.log('authentication failed');
    return;
  }
  callback(authClient);
}

function countUnique(iterable) {
  return new Set(iterable).size;
}

function convertToRupiah(angka) {
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
}

module.exports = investment;