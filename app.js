const express = require('express');
const app     = express();
const path    = require('path');

// ROUTE TO SPECIFIC CONTROLLER
const investment = require('./routes/investment.js');

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

// Trying to implement the middleware
  // app.use('/', (req, res, next) => {
  //   console.log('Going through the middleware first');
  //   next();
  // });

app.use(investment);

// APPLICATION LISTENER
const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
  console.log( `Server is up and listening on: ${PORT}`);
});